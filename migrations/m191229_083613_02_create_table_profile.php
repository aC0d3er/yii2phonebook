<?php

use yii\db\Migration;

class m191229_083613_02_create_table_profile extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_persian_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%profile}}', [
            'profile_ID' => $this->primaryKey(),
            'firstName' => $this->string(100)->notNull()->defaultValue(''),
            'lastName' => $this->string(100)->notNull()->defaultValue(''),
            'phone' => $this->bigInteger()->notNull()->defaultValue('0'),
            'address' => $this->text(),
            'isPublished' => $this->boolean()->notNull()->defaultValue('0'),
            'image_src_filename' => $this->string(),
            'image_web_filename' => $this->string(),
            'created_by' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addForeignKey('created_by', '{{%profile}}', 'created_by', '{{%user}}', 'user_ID', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%profile}}');
    }
}
