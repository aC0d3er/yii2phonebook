<p align="center">
    <a href="https://github.com/yiisoft" target="_blank">
        <img src="https://avatars0.githubusercontent.com/u/993323" height="100px">
    </a>
    <h1 align="center">Yii 2 PhoneBook Applications</h1>
    <br>
</p>

<p>
    first clone the project to your prefer path
</p>
<p>
    then run <bold>composer install</bold> in your command line
</p>
<p>
    at last run  <bold>php yii serve</bold>
</p>