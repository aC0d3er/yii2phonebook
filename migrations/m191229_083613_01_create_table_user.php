<?php

use yii\db\Migration;

class m191229_083613_01_create_table_user extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_persian_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'user_ID' => $this->primaryKey(),
            'username' => $this->string(100)->notNull()->defaultValue(''),
            'password' => $this->string()->notNull()->defaultValue(''),
            'auth_key' => $this->string()->notNull()->defaultValue(''),
            'access_token' => $this->string()->notNull()->defaultValue(''),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%user}}');
    }
}
