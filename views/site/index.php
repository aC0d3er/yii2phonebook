<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

<div class="jumbotron">
        <h1>PhoneBook Application!</h1>

        <p class="lead">the main focus on this app are in the backend</p>

        <p><a class="btn btn-lg btn-success" href="<?= \yii\helpers\Url::to('profile/show-public'); ?>">To See The Public Profiles</a></p>
    </div>

    <div class="body-content">
        <div class="row">
            <div class="col-lg 12">
                <h1>First config your DB then migrate the tables using this commnad</h1>
                <pre>
                    php yii migrate
                </pre>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <h1>create user</h1>

                <h3>to create user you must use Commnad Line with following command</h3>
                <pre>
                    php yii create
                </pre>
            </div>
        </div>           
    </div>
</div>
