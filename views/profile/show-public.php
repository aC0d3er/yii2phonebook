<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Public Profiles';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profile-index">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],


            'firstName',
            'lastName',
            'phone',
            'address:ntext',
            'createdBy.username',

            [
                'attribute' => 'Image',
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->image_web_filename!='')
                        return '<img src="'.Yii::$app->homeUrl. 'uploads/avatars/'.$model->image_web_filename.'" width="50px" height="auto">'; else return 'no image';
                },
            ],

        ]
    ]); ?>


</div>

