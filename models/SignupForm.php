<?php


namespace app\models;



use yii\db\ActiveRecord;
use yii\helpers\VarDumper;

class SignupForm extends ActiveRecord
{
    public $username;
    public $password;

    public static function tableName()
    {
        return 'user';
    }

    public function rules()
    {
        ['username' => 'unique'];
    }

    public function signup()
    {

            $user = new User();
            $user->username = $this->username;
            $user->password = \Yii::$app->security->generatePasswordHash($this->password);
            $user->access_token = \Yii::$app->security->generateRandomString();
            $user->auth_key = \Yii::$app->security->generateRandomString();


        if ($user->save()) {
            return true;
        }
        \Yii::error('User was not saved. ' . VarDumper::dumpAsString($user->errors));
        return false;
    }

}