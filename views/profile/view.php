<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Profile */

$this->title = $model->firstName .'-'.$model->lastName;
$this->params['breadcrumbs'][] = ['label' => 'Profiles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="profile-view">

    <h1><?= Html::encode($model->firstName.' '.$model->lastName) ?></h1>

  <?php if (! Yii::$app->user->isGuest): ?>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->profile_ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->profile_ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

  <?php endif; ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'firstName',
            'lastName',
            'phone',
            'address:ntext',
            'isPublished:boolean',
            'createdBy.username'
        ],
    ]) ?>

    <?php
    if ($model->image_web_filename!='') {
        echo '<br /><p><img src="'.Yii::$app->homeUrl. 'uploads/avatars/'.$model->image_web_filename.'" width="100" height="100"></p>';
    }
    ?>

</div>
