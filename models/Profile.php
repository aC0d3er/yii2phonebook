<?php

namespace app\models;

use yii\behaviors\BlameableBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "profile".
 *
 * @property int $profile_ID
 * @property string $firstName
 * @property string $lastName
 * @property int $phone
 * @property string|null $address
 * @property string $image_src_filename
 * @property string $image_web_filename
 * @property bool $isPublished
 * @property int $created_by
 *
 * @property User $createdBy
 */
class Profile extends ActiveRecord
{
    public $image;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'profile';
    }

    public function behaviors()
    {
        return [

            [
                'class' => BlameableBehavior::className(),
                'updatedByAttribute' => false,
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['phone', 'created_by'], 'integer'],
            [['address'], 'string'],
            [['created_by'], 'required'],
            [['isPublished'], 'boolean'],
            [['firstName', 'lastName'], 'string', 'max' => 100],
            [['image'], 'safe'],
            [['image'], 'file', 'extensions'=>'jpg, jpeg, png'],
            [['image'], 'file', 'maxSize'=>'100000000'],
            [['image_src_filename', 'image_web_filename'], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'user_ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'profile_ID' => 'Profile ID',
            'firstName' => 'First Name',
            'lastName' => 'Last Name',
            'phone' => 'Phone',
            'address' => 'Address',
            'image_src_filename' =>  'Filename',
            'image_web_filename' =>  'Pathname',
            'isPublished' => 'Is Published',
            'created_by' => 'Created By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['user_ID' => 'created_by']);
    }

    public function allPublic()
    {
        $public = Profile::find()->where(['isPublished' => true])->all();
    }
}
