<?php

namespace app\controllers;


use Yii;
use app\models\Profile;
use app\models\ProfileSearch;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ProfileController implements the CRUD actions for Profile model.
 */
class ProfileController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create','update','delete','index'],
                'rules' => [
                    [
                        'actions' => ['create','update','delete','index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }



    /**
     * Lists all Profile models.
     * @return mixed
     */
    public function actionIndex()
    {
        $query = Profile::find()->joinWith('createdBy')->where(['created_by' => Yii::$app->user->getId()]);
        $searchModel = new ProfileSearch();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => 10],
        ]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Profile model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        if ($model->created_by !== Yii::$app->user->id)
        {
            throw new ForbiddenHttpException('you do not have permission to update this profile');
        }
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Profile model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Profile();


        if ($model->load(Yii::$app->request->post())) {
            $model->created_by = Yii::$app->user->id;


            $image = UploadedFile::getInstance($model, 'image');
            if (!is_null($image)) {
                $model->image_src_filename = $image->name;

                // generate a unique file name to prevent duplicate filenames
                $model->image_web_filename = Yii::$app->security->generateRandomString().".".$image->getExtension();
                // the path to save file, you can set an uploadPath
                // in Yii::$app->params (as used in example below)
                Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/uploads/avatars/';
                $path = Yii::$app->params['uploadPath'] . $model->image_web_filename;
                $image->saveAs($path);
            }
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->profile_ID]);
            }  else {

                var_dump ($model->getErrors()); die();
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);

//        if ($model->load(Yii::$app->request->post() && $model->save()) ) {
//
//
//            $avatar = UploadedFile::getInstance($model, 'avatar');
//            $avatarName = time().'_avatar.'.$avatar->getExtension();
//            $avatar->saveAs(Yii::$app->basePath.'/uploads/avatars/'.$avatarName);
//            $model->avatar = $avatarName;
//            $model->save();
//            return $this->redirect(['view', 'id' => $model->profile_ID]);
//        }
//
//        return $this->render('create', [
//            'model' => $model,
//        ]);


    }

    /**
     * Updates an existing Profile model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->created_by !== Yii::$app->user->id)
        {
            throw new ForbiddenHttpException('you do not have permission to update this profile');
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->profile_ID]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Profile model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if ($model->created_by !== Yii::$app->user->id)
        {
            throw new ForbiddenHttpException('you do not have permission to update this profile');
        }
        $model->delete();
        return $this->redirect(['index']);
    }

    public function actionShowPublic()
    {
        $query = Profile::find()->where(['isPublished' => true]);
        $searchModel = new ProfileSearch();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => 10],
        ]);

        return $this->render('show-public' , [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }



    /**
     * Finds the Profile model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Profile the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Profile::findOne(['profile_ID' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
